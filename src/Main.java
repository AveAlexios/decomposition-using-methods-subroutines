
import java.util.Scanner;

public class Main {
//
//This program is for Tasks 1-4 in module 'Decomposition using methods'
//
// Main logic of the tasks runs in Main method. There are also 5 another methods were written,
//which are responsible for the corresponding math operations - they can be called from the Main method.
// Some of those 5 methods can pass values to each other and some not.
//One input method and one validation method were written as well.


// First of all you need to input 2,3,4 and hence set the length to the array which
// will be passed to corresponding methods as a parameter.


//Inputting integer 2 runs method to find the Greatest Common Divisor and the Least Common Multiple
// of two next numbers
//Inputting 3 runs method to find the Least Common Multiple of three next numbers
//Inputting 4 finds the Greatest Common Divisor and the Least Common Multiple of next four numbers
//
//Method calculating the Sum of Factorials runs by itself in any case
//
//
//

//Math methods
    public static int greatestCommonDivisorOfTwo(int... array) {
        int commonDivisor = 1;
        int x = Math.abs(array[0]);
        int y = Math.abs(array[1]);
        for(int i = 1; i <= Math.min(x, y); i++){
            if((x % i == 0) && (y % i == 0)) {
                commonDivisor = i;
            }
        }return commonDivisor;
    }

    public static int leastCommonMultipleOfTwo(int... array){
        int x = Math.abs(array[0]);
        int y = Math.abs(array[1]);
        return x * y / greatestCommonDivisorOfTwo(array);
    }

    public static int leastCommonMultipleOfThree(int... array) {
        int commonMultipleOfTree = 1;
        int divisor = 2;
        while (true) {
            int counter = 0;
            boolean divisible = false;
            for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                    return 0;
                } else if (array[i] < 0) {
                    array[i] = array[i] * (-1);
                }
                if (array[i] == 1) {
                    counter++;
                }

                if (array[i] % divisor == 0) {
                    divisible = true;
                    array[i] = array[i] / divisor;
                }
            }

            if (divisible) {
                commonMultipleOfTree = commonMultipleOfTree * divisor;
            } else {
                divisor++;
            }

            if (counter == array.length) {
                return commonMultipleOfTree;
            }
        }
    }

    public static int greatestCommonDivisorOfFour(int ... array){
        int x = array[0];
        int y = array[1];
        int z = array[2];
        int w = array[3];
        int commonDivisor = 1;
        int minimal = Math.min( Math.min(z,w), Math.min(x,y) );
        for(int i = 1; i <= minimal; i++){
            if((x % i == 0) && (y % i == 0) && (z % i == 0) && (w % i == 0)) {
                commonDivisor = i;
            }
        }return commonDivisor;
    }

    public static void sumOfFactorials(int x){
        int sum = 0;
        for(int i = 1; i < 10; i++){
            if(i %  2 != 0){
                int factorial = 1;
                for(int j = 1; j <=i ; j++){
                    factorial = factorial * j;

                }
                sum = sum + factorial;
            }
        }
        System.out.println();
        System.out.println("AND BTW - Sum of factorials of all odd numbers from 1 to 9 is " + sum);
    }


//Input Methods
    public static int validationInput(){
        Scanner scanner = new Scanner(System.in);
        int number;
        do {
            System.out.print("Please enter an Integer from 2 to 4: ");
            while (!scanner.hasNextInt())  {
                String input = scanner.next();
                System.out.printf("\"%s\" is not an Integer. %n", input);
            }
            number = scanner.nextInt();
        } while (number < 2 || number > 4);
        System.out.printf("You have entered a number : %d.%n ", number);
        return number;
    }
    public static int[] inputOtherNumbersFromConsole(){

        Scanner sc = new Scanner(System.in);
        int n = validationInput();
        int[] inputArray = new int[n];

        if( n == 2 ){
            for(int i = 0; i < n; i++) {
                inputArray[i] = sc.nextInt();
            }
        return inputArray;
        }

        else if (n == 3) {
            for(int i = 0; i < n; i++) {
                inputArray[i] = sc.nextInt();
            }
        return inputArray;
        }

        else if (n >= 4) {
            for (int i = 0; i < 4; i++) {
                inputArray[i] = sc.nextInt();
            }
        return inputArray;
        }
        return inputArray;
    }


//Main methods,which is responsible for the logic - which method to call
// after inputting the very first number
    public static void main(String[] args) {


        int[] array = inputOtherNumbersFromConsole();

        if(array.length == 2){
            System.out.println("Greatest common divisor of " + array[0] + " and " + array[1]
                + " : "+ greatestCommonDivisorOfTwo(array));
                System.out.println("Least common multiple of " + array[0] + " and " + array[1]
                + " : " + leastCommonMultipleOfTwo(array));
                sumOfFactorials(10);

            } else if (array.length == 3) {
                System.out.println("Least common multiple of " + array[0] + ", "
                + array[1] + " and " + array[2] + " : " + leastCommonMultipleOfThree(array));
                sumOfFactorials(10);

            } else if (array.length >= 4) {
                System.out.println("Greatest common divisor of " + array[0] + ", "
                    + array[1] + ", " + array[2] + " and " + array[3] + " : " + greatestCommonDivisorOfFour(array));
            sumOfFactorials(10);
        }
    }
}
